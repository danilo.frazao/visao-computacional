import cv2
import numpy as np
  
# Read the images
img = cv2.imread("../imagens/usa.jpg")

# Convert Image to Image HSV
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
  
# Defining lower and upper bound HSV values
low_red = np.array([166, 84, 141])
high_red = np.array([186, 255, 255])
  
# Defining mask for detecting color
mask = cv2.inRange(hsv, low_red, high_red)
  
# Display Image and Mask
cv2.imshow("Image", img)
cv2.imshow("Mask", mask)
  
# Make python sleep for unlimited time
cv2.waitKey(0)