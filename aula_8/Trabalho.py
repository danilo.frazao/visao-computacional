# importação das bibliotecas necessárias
import cv2
import time
import numpy as np

# inicialização da câmera
cap = cv2.VideoCapture(0)

# captura do primeiro frame
_, background = cap.read()
time.sleep(2)
_, background = cap.read()

cv2.namedWindow("janela")

# definiçõ do tamanho dos kernels
open_kernel = np.ones((5,5),np.uint8)
close_kernel = np.ones((7,7),np.uint8)
dilation_kernel = np.ones((10, 10), np.uint8)


# definição da função para retirar ruído da máscara
def filter_mask(mask):
    close_mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, close_kernel)
    open_mask = cv2.morphologyEx(close_mask, cv2.MORPH_OPEN, open_kernel)
    dilation = cv2.dilate(open_mask, dilation_kernel, iterations= 1)

    return dilation

def mause(pos):
    pass

# criação dos sliders para que o usuário possa escolher as cores
cv2.createTrackbar("H_min", "janela", 0, 360, mause)
cv2.createTrackbar("H_max", "janela", 0, 360, mause)
cv2.createTrackbar("S_min", "janela", 0, 255, mause)
cv2.createTrackbar("S_max", "janela", 0, 255, mause)
cv2.createTrackbar("V_min", "janela", 0, 255, mause)
cv2.createTrackbar("V_max", "janela", 0, 255, mause)

while cap.isOpened():
    # capturar cada frame enquanto estiver em loop
    ret, frame = cap.read()

    # Acesso aos valores das cores escolhidas pelo usuário
    h_min = cv2.getTrackbarPos("H_min", 'janela')
    s_min = cv2.getTrackbarPos("S_min", 'janela')
    v_min = cv2.getTrackbarPos("V_min", 'janela')
    h_max = cv2.getTrackbarPos("H_max", 'janela')
    s_max = cv2.getTrackbarPos("S_max", 'janela')
    v_max = cv2.getTrackbarPos("V_max", 'janela')


    # conversão para hsv
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # encontrando as cores dentro dos limites
    mask = cv2.inRange(hsv, np.array([h_min, s_min, v_min]), np.array([h_max, s_max, v_max]))
    # mask = cv2.inRange(hsv, np.array([66, 1, 32]), np.array([99, 43, 103]))

    # remoção dos ruídos da máscara
    mask = filter_mask(mask)

    # criação de máscara inversa
    inverse_mask = cv2.bitwise_not(mask)

    # obtendo as regiões do frame onde a "capa" não está presente
    # para isso usamos a máscara inversa
    current_background = cv2.bitwise_and(frame, frame, mask=inverse_mask)

    # obtendo as regiões do frame onde a capa está presente
    cloak = cv2.bitwise_and(background, background, mask=mask)

    # Combinação do background atual com a região da capa para gerar o frame final
    combined = cv2.add(cloak, current_background)

    # mostrar resultado
    cv2.imshow('janela', combined)
    # cv2.imshow('Sem efeito', frame)

    # possibilita o usuário interromper o programa
    k = cv2.waitKey(30)
    if k == ord("q"):
        break

