from matplotlib import pyplot as plt
import numpy as np
import cv2

img = cv2.imread("disney.jpeg")
img = img[::3,::3] # Diminui a imagem
suave = np.vstack([
 np.hstack([img,
    cv2.GaussianBlur(img, ( 3, 3), 0)]),
    np.hstack([cv2.GaussianBlur(img, ( 5, 5), 0),
    cv2.GaussianBlur(img, ( 7, 7), 0)]),
    np.hstack([cv2.GaussianBlur(img, ( 9, 9), 0),
    cv2.GaussianBlur(img, (11, 11), 0)]),
 ])
cv2.imshow("Imagem original e suavisadas pelo filtro Gaussiano", suave)
cv2.waitKey(0)