import cv2
import numpy as np   
    
canvas = np.ones((300, 400, 3)) * 255 #imagem 400x300, com fundo branco e 3 canais para as cores

# desenha a linha diagonal
azul = (255,0,0)
cv2.line(canvas, (0, 0), (400, 300), azul)

# desenha a linha vertical
verde = (0, 255, 0)
cv2.line(canvas, (200, 0), (200, 300), verde, 3)

cv2.imshow("Canvas", canvas)
cv2.waitKey(0)


