import numpy as np
import cv2

img = cv2.imread('../imagens/disney.jpeg')
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

width = 350
height = 450
dim = (width, height)
 
# resize image
resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)


#redimensionamento da imagem
suave = cv2.GaussianBlur(resized, (7, 7), 0)
canny1 = cv2.Canny(suave, 20, 120)
canny2 = cv2.Canny(suave, 70, 200)
resultado = np.vstack([
    np.hstack([resized, suave ]),
    np.hstack([canny1, canny2])
])

cv2.imshow("Detector de Bordas Canny", resultado)
cv2.waitKey(0)


