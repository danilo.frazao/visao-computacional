import cv2
import numpy as np
 
captura = cv2.VideoCapture(0)
 
while(1):
    ret, frame = captura.read()
    frameCinza = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    (t,binario) = cv2.threshold(frameCinza, 160, 255, cv2.THRESH_BINARY)
    (t,binario_inv) = cv2.threshold(frameCinza, 160, 255, cv2.THRESH_BINARY_INV)

    kernel = np.ones((5, 5), np.uint8)
    erosao = cv2.erode(frameCinza, kernel)
    dilatacao = cv2.dilate(frameCinza, kernel) 
    abertura = cv2.morphologyEx(frameCinza, cv2.MORPH_OPEN, kernel)
    fechamento = cv2.morphologyEx(frameCinza, cv2.MORPH_CLOSE, kernel)
    gradiente = cv2.morphologyEx(frameCinza, cv2.MORPH_GRADIENT, kernel)

    cv2.imshow("Video", frame)
    cv2.imshow("Video2", frameCinza)
    cv2.imshow("Binario", binario)
    cv2.imshow("Binario invertido", binario_inv)
    cv2.imshow("Erosao", erosao)
    cv2.imshow("Dilatacao", dilatacao)
    cv2.imshow("Abertura", abertura)
    cv2.imshow("fechamento", fechamento)
    cv2.imshow("Gradiente", gradiente)
   
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break
 
captura.release()
cv2.destroyAllWindows()