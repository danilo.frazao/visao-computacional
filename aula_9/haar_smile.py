import cv2
face_detector = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
smile_dectector = cv2.CascadeClassifier('haarcascade_smile.xml')

# reading the input image now
cap = cv2.VideoCapture(0)
while cap.isOpened():
    _, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = face_detector.detectMultiScale(gray, 1.3, 5)

    for (x,y, w, h) in faces:
        cv2.rectangle(frame, (x, y), ((x + w), (y + h)), (255, 0, 0), 2)
        roi_gray = gray[y:y+h,x:x+w]
        roi_color = frame[y:y+h, x:x+w]
        smiles = smile_dectector.detectMultiScale(roi_gray, 1.8, 20)
        for (ex,ey, ew, eh) in smiles:
            cv2.rectangle(roi_color, (ex,ey), (ex+ew, ey+eh), (0,255,0), 5)
    cv2.imshow("window", frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
frame.release()
cv2.destroyAllWindows()